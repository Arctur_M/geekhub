package hw3_2;

class Task {
    private final String name;
    private final String category;

    Task(String name, String category) {
        this.name = name;
        this.category = category;
    }

    private String getName() {
        return name;
    }

    String getCategory() {
        return category;
    }

    @Override
    public String toString() {
        return "Task name: " + getName()  + "; Task category: " + getCategory();
    }
}
