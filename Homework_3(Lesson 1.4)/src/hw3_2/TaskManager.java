package hw3_2;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Set;

public interface TaskManager {
    void add(LocalDateTime date, Task task);

    void remove(LocalDate date);

    Set<String> getCategories();

    Map<String, List<Task>> getTaskByCategories(String... categories);

    List<Task> getTaskByCategory(String category);

    List<Task> getTasksForToday();
}
