package hw3_2;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;

public class TaskManagerImpl implements TaskManager {
    private Map<LocalDate, List<Task>> tasks = new HashMap<>();

    @Override
    public void add(LocalDateTime date, Task task) {
        List<Task> tasks = this.tasks.get(date);
        if (tasks == null)
            tasks = new ArrayList<Task>();
        tasks.add(task);
        this.tasks.put(date.toLocalDate(), tasks);
    }

    @Override
    public void remove(LocalDate date) {
        this.tasks.remove(date);
    }

    @Override
    public Set<String> getCategories() {
        Set<String> categories = new HashSet<>();

        for (List<Task> tasks : this.tasks.values()) {
            for (Task task : tasks) {
                categories.add(task.getCategory());
            }
        }
        return categories;
    }

    @Override
    public Map<String, List<Task>> getTaskByCategories(String... categories) {
        Map<String, List<Task>> listMap = new HashMap<>();

        for (String category : categories) {
            listMap.put(category, getTaskByCategory(category));
        }
        return listMap;
    }

    @Override
    public List<Task> getTaskByCategory(String category) {
        List<Task> taskByCategory = new ArrayList<>();

        for (List<Task> tasks : this.tasks.values()) {
            for (Task task : tasks) {
                taskByCategory.add(task);
            }
        }
        return taskByCategory;
    }

    @Override
    public List<Task> getTasksForToday() {
        return this.tasks.get(LocalDate.now());
    }
}
