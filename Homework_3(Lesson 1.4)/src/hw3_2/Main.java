package hw3_2;

import java.time.LocalDateTime;

public class Main {
    public static void main(String[] args) {
        TaskManagerImpl taskManager = new TaskManagerImpl();

        taskManager.add(LocalDateTime.now().plusDays(2), new Task("asdfasdf0", "asdfasdf0"));
        taskManager.add(LocalDateTime.now(),             new Task("asdfasdf1", "asdfasdf1"));
        taskManager.add(LocalDateTime.now(),             new Task("asdfasdf11", "asdfasdf11"));
        taskManager.add(LocalDateTime.now().plusDays(1), new Task("asdfasdf2", "asdfasdf2"));

        System.out.println(taskManager.getCategories());
        System.out.println(taskManager.getTasksForToday());
    }
}
