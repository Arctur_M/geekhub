package hw3_1;

class Product {
    private String name;
    private double price;
    private int quantityOnHands;

    Product(String name, double price, int quantityOnHands) {
        this.name = name;
        this.price = price;
        this.quantityOnHands = quantityOnHands;
    }

    String getName() {
        return name;
    }

    double getPrice() {
        return price;
    }

    int getQuantityOnHands() {
        return quantityOnHands;
    }

    double getFullPrice() {
        return this.quantityOnHands * this.price;
    }
}