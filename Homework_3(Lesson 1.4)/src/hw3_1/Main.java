package hw3_1;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Inventory i = new Inventory();
        boolean flag = true;

        String pName = "Enter product name: \n";
        String pPrice = "Enter product price: \n";
        String pQuantity = "Enter quantity of product: \n";

        while (flag) {
            System.out.println("Choose your option:");
            System.out.println("    Enter 1 to add new product.");
            System.out.println("    Enter 2 to remove a product from the inventory.");
            System.out.println("    Enter 3 to show all products in the inventory ");
            System.out.println("    Enter 4 to calculate summary price of the inventory ");
            System.out.println("    Enter 0 to quit the application.");

            System.out.println("Enter option: ");
            String prod = scanner.nextLine();

            System.out.println();
            switch (prod) {
                case "1":
                    String prodName = checkForString(scanner, pName);
                    int prodPrice = checkForInt(scanner, pPrice);
                    int prodQuantity = checkForInt(scanner, pQuantity);
                    i.addProduct(prodName, prodPrice, prodQuantity);
                    break;
                case "2":
                    prodName = checkForString(scanner, pName);
                    boolean prodRemove = i.removeProduct(prodName);
                    checkProductInInventory(prodRemove);
                    break;
                case "3":
                    i.showInventoryProducts();
                    break;
                case "4":
                    i.sumInventoryProducts();
                    break;
                case "0":
                    flag = false;
                    break;
            }
        }
    }

    private static int checkForInt(Scanner scan, String q) {
        int value;
        System.out.print(q);
        while (true) {
            try {
                value = Integer.parseInt(scan.nextLine());
                break;
            } catch (NumberFormatException e) {
                System.out.println("Error: " + e.getMessage());
                System.out.print("Try again! " + q);
            }
        }
        return value;
    }

    private static String checkForString(Scanner scan, String q) {
        String value;
        System.out.print(q);
        while (true) {
            try {
                value = String.valueOf(scan.nextLine());
                break;
            } catch (NumberFormatException e) {
                System.out.println("Error: " + e.getMessage());
                System.out.print("Try again! " + q);
            }
        }
        return value;
    }

    private static void checkProductInInventory(boolean status) {
        if (!status) {
            System.out.println("Error. No such product in inventory!");
        }
    }
}
