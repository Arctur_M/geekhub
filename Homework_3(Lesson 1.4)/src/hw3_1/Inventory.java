package hw3_1;

import java.util.HashMap;
import java.util.Map;

class Inventory {
    private Map<String, Product> productMap;

    Inventory() {
        this.productMap = new HashMap<>();
    }

    boolean addProduct(String name, int price, int quantity) {
        if (this.productMap.containsKey(name)) {
            return false;
        }
        this.productMap.put(name, new Product(name, price, quantity));
        return true;
    }

    boolean removeProduct(String name) {
        this.productMap.remove(name);
        return false;
    }

    void sumInventoryProducts() {
        double totalProductsSum = 0;
        for (Product product : this.productMap.values()) {
            totalProductsSum = totalProductsSum + (product.getFullPrice());
        }
        System.out.println("Total value of all products : " + totalProductsSum);
    }

    void showInventoryProducts() {
        System.out.println();
        for (Product product : this.productMap.values()) {
            System.out.printf("Product name : %s, price : %s, quantity : %d%n", product.getName(), product.getPrice(),
                    product.getQuantityOnHands());
        }
    }
}
