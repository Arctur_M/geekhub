package hw2_2.Vechicles;

public class SolarCar extends Car {
    private long batteryCharge;
    private int wheels = getWheels();

    private void defaultSolarCar() {
        batteryCharge = 40000;
    }

    private long getBatteryCharge() {
        return batteryCharge;
    }

    public void accelerate(int speed) {
        super.setSpeed(speed);
        defaultSolarCar();
        wheels = speed * 100;
        batteryCharge -= speed;
    }

    @Override
    public String toString() {
        return "SolarCar: " + "batteryCharge= " + getBatteryCharge() + ", rotationWheels= " + wheels
                + ", speed= " + getSpeed() + ", direction= " + getDirection();
    }
}
