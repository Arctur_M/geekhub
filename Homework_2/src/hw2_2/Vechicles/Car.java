package hw2_2.Vechicles;

public class Car extends Vehicle {
    private int engine = getEngine();
    private int wheels = getWheels();
    private double gasTank = getGasTank();

    @Override
    public void accelerate(int speed) {
        super.setSpeed(speed);
        engine = speed * 10;
        wheels = speed * 100;
        gasTank -= engine * 0.01;
    }

    @Override
    public void brake() {
        super.setSpeed(0);
        engine = 0;
        wheels = 0;
    }

    @Override
    public String toString() {
        return "Car: " + "engine= " + engine + ", rotationWheels= " + wheels
                + ", gasTank= " + gasTank + ", " + super.toString();
    }
}
