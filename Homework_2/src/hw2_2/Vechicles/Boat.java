package hw2_2.Vechicles;

import hw2_2.Driveable;

public class Boat extends Vehicle implements Driveable {
    private int speed = getSpeed();
    private double gasTank = getGasTank();
    private int engine = getEngine();

    public int getSpeed() {
        return speed;
    }

    @Override
    public void accelerate(int speed) {
        super.setSpeed(speed);
        engine = speed * 10;
        gasTank -= engine * 0.05;
    }

    @Override
    public void brake() {
        super.setSpeed(0);
        engine = 0;
    }

    @Override
    public String toString() {
        return "Boat: " + "engine= " + engine + ", gasTank= " + gasTank + ", " + super.toString();
    }
}
