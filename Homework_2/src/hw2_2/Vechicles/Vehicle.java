package hw2_2.Vechicles;

import hw2_2.Direction;
import hw2_2.Driveable;

public abstract class Vehicle implements Driveable {
    private int engine;
    private int wheels;
    private double gasTank;
    private int speed;
    private Direction direction;

    int getEngine() {
        return engine;
    }

    void setEngine(int engine) {
        this.engine = engine;
    }

    int getWheels() {
        return wheels;
    }

    void setWheels(int wheels) {
        this.wheels = wheels;
    }

    private void defaultGasTank() {
        gasTank = 100;
    }

    double getGasTank() {
        defaultGasTank();
        return gasTank;
    }

    int getSpeed() {
        return speed;
    }

    void setSpeed(int speed) {
        this.speed = speed;
    }

    Direction getDirection() {
        return direction;
    }

    private void setDirection(Direction direction) {
        this.direction = direction;
    }

    @Override
    public void brake() {
        setSpeed(0);
    }

    @Override
    public void turn(Direction direction) {
        setDirection(direction);
    }

    public String toString() {
        return "speed= " + speed + ", direction= " + direction;
    }
}
