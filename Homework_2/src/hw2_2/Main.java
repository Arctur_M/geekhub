package hw2_2;

import hw2_2.Vechicles.Boat;
import hw2_2.Vechicles.Car;
import hw2_2.Vechicles.SolarCar;
import hw2_2.Vechicles.Vehicle;

public class Main {
    public static void main(String[] args) {
        Vehicle boat = new Boat();

        boat.accelerate(5);
        boat.turn(Direction.EAST);
        System.out.println(boat);
        boat.turn(Direction.SOUTH);
        boat.brake();
        System.out.println(boat);

        Vehicle car = new Car();

        System.out.println(car);
        car.accelerate(70);
        car.turn(Direction.NORTH);
        System.out.println(car);
        car.brake();
        System.out.println(car);

        Vehicle solarCar = new SolarCar();

        System.out.println(solarCar);
        solarCar.accelerate(50);
        solarCar.turn(Direction.WEST);
        System.out.println(solarCar);
        solarCar.brake();
        System.out.println(solarCar);
    }
}
