package hw2_2;

public interface Driveable {
    void accelerate(int speed);
    void turn(Direction direction);
    void brake();
}
