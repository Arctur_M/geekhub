package hw2_1;

import hw2_1.figures.Circle;
import hw2_1.figures.Rectangle;
import hw2_1.figures.Square;
import hw2_1.figures.Triangle;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        double a, b, c;
        System.out.println("Enter the figure");
        Scanner in = new Scanner(System.in);
        String shape = in.next();

        switch (ShapeType.valueOf(shape.toUpperCase())) {
            case CIRCLE:
                System.out.println("Enter the radius");
                double r = in.nextDouble();
                Shape circle = new Circle(r);
                printResult(circle.calculateArea(), circle.calculatePerimeter());
                break;
            case SQUARE:
                System.out.println("Enter the side");
                a = in.nextDouble();
                Shape square = new Square(a);
                printResult(square.calculateArea(), square.calculatePerimeter());

                System.out.println("Triangle");
                Triangle triangle = Square.getInnTriangle(a);
                printResult(triangle.calculateArea(), triangle.calculatePerimeter());
                break;
            case RECTANGLE:
                System.out.println("Enter the first side");
                a = in.nextDouble();
                System.out.println("Enter the second side");
                b = in.nextDouble();
                Shape rectangle = new Rectangle(a, b);
                printResult(rectangle.calculateArea(), rectangle.calculatePerimeter());

                System.out.println("Triangle");
                triangle = Rectangle.getInnerTriangle(a, b);
                printResult(triangle.calculateArea(), triangle.calculatePerimeter());
                break;
            case TRIANGLE:
                System.out.println("Enter the first side");
                a = in.nextDouble();
                System.out.println("Enter the second side");
                b = in.nextDouble();
                System.out.println("Enter the third side");
                c = in.nextDouble();
                triangle = new Triangle(a, b, c);
                printResult(triangle.calculateArea(), triangle.calculatePerimeter());
                break;
            default:
                System.out.println("Figure not implemented");
                break;
        }
        in.close();
    }

    private static void printResult(double area, double perimeter) {
        System.out.println("Area " + area);
        System.out.println("Perimeter " + perimeter);
    }
}
