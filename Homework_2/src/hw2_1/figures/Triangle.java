package hw2_1.figures;

import hw2_1.Shape;

public class Triangle implements Shape {
    private double a;
    private double b;
    private double c;

    public Triangle(double a, double b, double c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    @Override
    public double calculateArea() {
        double perimeter = this.a + this.b + this.c;
        return Math.sqrt(perimeter * (perimeter - this.a) * (perimeter - this.b) * (perimeter - this.c));
    }

    @Override
    public double calculatePerimeter() {
        return this.a + this.b + this.c;
    }

}
