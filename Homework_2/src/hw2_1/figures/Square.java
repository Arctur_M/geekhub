package hw2_1.figures;

import hw2_1.Shape;

public class Square implements Shape {
    private double a;

    public Square(double a) {
        this.a = a;
    }

    @Override
    public double calculateArea() {
        return this.a * this.a;
    }

    @Override
    public double calculatePerimeter() {
        return 4 * this.a;
    }

    public static Triangle getInnTriangle(double a) {
        double c = Math.sqrt((a * a) * 2);
        Triangle triangle = new Triangle(a, a, c);
        triangle.calculateArea();
        triangle.calculatePerimeter();
        return triangle;
    }
}
