package hw2_1.figures;

import hw2_1.Shape;

public class Rectangle implements Shape {
    private double a;
    private double b;

    public Rectangle(double a, double b) {
        this.a = a;
        this.b = b;
    }

    @Override
    public double calculateArea() {
        return this.a * this.b;
    }

    @Override
    public double calculatePerimeter() {
        return 2 * (this.a + this.b);
    }

    public static Triangle getInnerTriangle(double a, double b) {
        double c = Math.sqrt(a * a + b * b);
        Triangle triangle = new Triangle(a, b, c);
        triangle.calculateArea();
        triangle.calculatePerimeter();
        return triangle;
    }
}