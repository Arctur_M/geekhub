package hw2_1.figures;

import hw2_1.Shape;

public class Circle implements Shape {
    private double r;

    public Circle(double r) {
        this.r = r;
    }

    @Override
    public double calculateArea() {
        return this.r * this.r * Math.PI;
    }

    @Override
    public double calculatePerimeter() {
        return 2 * this.r * Math.PI;
    }
}
