package hw2_1;

public interface Shape {
    double calculateArea();
    double calculatePerimeter();
}
