package com.gh.hw4_2;

import java.util.Collections;
import java.util.List;

public class ListSorter {

    public static <T extends Comparable> List<T> sortByBubbles(List<T> elements, Direction direction) {
        for (int i = 0; i < elements.size(); i++) {
            for (int j = i + 1; j < elements.size(); j++) {
                if (elements.get(i).compareTo(elements.get(j)) == direction.getDirection()) {
                    swapValues(elements, i, j);
                }
            }
        }
        return elements;
    }

    public static <T extends Comparable> List<T> sortByInsertion(List<T> elements, Direction direction) {
        for (int i = 1; i < elements.size(); i++) {
            T index = elements.get(i);
            int j = i;

            while (j > 0 && (elements.get(j - 1).compareTo(index) == direction.getDirection())) {
                Collections.swap(elements, j, j - 1);
                j--;
            }
        }
        return elements;
    }

    public static <T extends Comparable> List<T> sortBySelection(List<T> elements, Direction direction) {
        for (int i = 0; i < elements.size() - 1; i++) {
            int minValue = i;
            for (int j = i; j < elements.size(); j++) {
                if ((elements.get(minValue).compareTo(elements.get(j))) == direction.getDirection()) {
                    minValue = j;
                }
                swapValues(elements, i, minValue);
            }
        }
        return elements;
    }

    private static <T extends Comparable> void swapValues(List<T> elements, int i, int j) {
        T currentValue;
        currentValue = elements.get(i);
        elements.set(i, elements.get(j));
        elements.set(j, currentValue);
    }
}