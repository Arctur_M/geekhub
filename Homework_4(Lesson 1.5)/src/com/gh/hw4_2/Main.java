package com.gh.hw4_2;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();
        list.add(3);
        list.add(32);
        list.add(5);
        list.add(2233);
        list.add(3);
        list.add(323);
        System.out.println(list);

        System.out.println(ListSorter.sortByBubbles(list, Direction.ASC));
        System.out.println(ListSorter.sortByBubbles(list, Direction.DESC));
        System.out.println(ListSorter.sortByInsertion(list, Direction.ASC));
        System.out.println(ListSorter.sortByInsertion(list, Direction.DESC));
        System.out.println(ListSorter.sortBySelection(list, Direction.ASC));
        System.out.println(ListSorter.sortBySelection(list, Direction.DESC));
    }
}
