package com.gh.hw4_1;

public interface List<E> {
    void add(E element);

    boolean remove(E element);

    boolean remove(int index);

    E get(int index);

    int size();

    boolean isEmpty();

    void print();
}
