package com.gh.hw4_1;

public class LinkedList<E> implements List<E> {
    private Node<E> head;
    private int size;

    @Override
    public void add(E element) {
        Node<E> ref = head;

        if (ref == null) {
            head = new Node<>(element, null);
        } else {
            while (ref.getNext() != null) {
                ref = ref.getNext();
            }
            ref.setNext(new Node<>(element, null));
        }
        size++;
    }

    @Override
    public boolean remove(E element) {
        if (head == null)
            return false;
        if (head.getValue() == element) {
            head = head.getNext();
            size--;
            return true;
        } else {
            Node<E> current = head;
            Node<E> prev = null;

            while (current != null && current.getValue() != element) {
                prev = current;
                current = current.getNext();
            }
            size--;
            if (prev != null) {
                if (current != null) {
                    prev.setNext(current.getNext());
                }
            }
            return true;
        }
    }

    @Override
    public boolean remove(int index) {
        if (index < 0 || index > size - 1) {
            throw new IndexOutOfBoundsException("Index : " + index + ", size : " + size);
        }

        E value = this.get(index);

        return this.remove(value);
    }

    @Override
    public E get(int index) {
        if (index < 0 || index > size - 1) {
            throw new IndexOutOfBoundsException("Index : " + index + ", size : " + size);
        }

        Node<E> value = head;

        for (int i = 0; i < index; i++) {
            value = value.getNext();
        }

        return value.getValue();
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return head == null;
    }

    @Override
    public void print() {
        Node<E> element = head;
        while (element != null) {
            System.out.println(element.getValue() + " ");
            element = element.getNext();
        }
    }
}