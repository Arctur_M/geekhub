package com.geekhub.hw1;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Enter phone number: ");
        long number = in.nextLong();

        while (!validate(number)) {
            System.out.println("Phone number is incorrect. Please enter phone number: ");
            number = in.nextLong();
            System.out.println(validate(number));
        }

        in.close();
        System.out.println("Number is correct.");
        long resultOfFirstCalculation = sum(number);
        System.out.println("First calculation: " + resultOfFirstCalculation);
        long resultOfSecondCalculation = sum(resultOfFirstCalculation);
        System.out.println("Second calculation: " + resultOfSecondCalculation);
        long resultOfFinalCalculation = sumToOneNumber(number);

        if (resultOfFinalCalculation == 1) {
            System.out.println("Final result is: One");
            return;
        }
        if (resultOfFinalCalculation == 2) {
            System.out.println("Final result is: Two");
            return;
        }
        if (resultOfFinalCalculation == 3) {
            System.out.println("Final result is: Three");
            return;
        }
        if (resultOfFinalCalculation == 4) {
            System.out.println("Final result is: Four");
            return;
        }
        System.out.print("Final result is: " + resultOfFinalCalculation);
    }

    private static boolean validate(long number) {
        if (number < 0) {
            return false;
        }
        int count = (number == 0) ? 1 : 0;
        while (number != 0) {
            count++;
            number /= 10;
        }
        switch (count) {
            case 12:
                return true;
            case 11:
                return true;
            case 9:
                return true;
        }
        return false;
    }

    private static long sumToOneNumber(long number) {
        while (number > 10) {
            number = sum(number);
        }

        return number;
    }

    private static long sum(long number) {
        if (number == 0) {
            return 0;
        }

        long result = 0;
        result += number % 10;
        number = number / 10;

        return result + sum(number);
    }
}
