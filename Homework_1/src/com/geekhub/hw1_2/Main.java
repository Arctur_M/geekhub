package com.geekhub.hw1_2;

import java.util.Scanner;

public class Main {
    public static void main (String[] args){

        Scanner input = new Scanner(System.in);
        System.out.println("Enter number of words: ");
        int numberOfStr = input.nextInt();
        System.out.println("Enter words: ");
        String[] strs = new String[numberOfStr];

        for (int i=0; i < numberOfStr; i++) {
            strs[i] = input.next();
        }

        for (int i=0; i<numberOfStr; i++) {
            if ((strs[i].length() > 5) && strs[i].length() <= 100){
                System.out.println(strs[i].substring(0, 1) + (strs[i].length() - 2) +
                        strs[i].substring(strs[i].length() - 1));
            } else System.out.println(strs[i]);
        }
    }
}

