package hw.task1;

import java.util.*;
import java.util.function.BinaryOperator;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

public class CollectionUtils {
    private CollectionUtils() {
    }

    public static <E> List<E> generate(Supplier<E> generator, int count) {
        return null;
    }

    public static <E> List<E> filter(List<E> elements, Predicate<E> filter) {
        List<E> list = new ArrayList<>();

        for (E currentElement : elements) {
            if (filter.test(currentElement)) {
                list.add(currentElement);
            }
        }
        return list;
    }

    public static <E> boolean anyMatch(List<E> elements, Predicate<E> predicate) {
        if (elements.isEmpty()) {
            return false;
        }

        for (E currentElement : elements) {
            if (predicate.test(currentElement)) {
                return true;
            }
        }
        return false;
    }

    public static <E> boolean allMatch(List<E> elements, Predicate<E> predicate) {
        int count = 0;

        for (E currentElement : elements) {
            count = Collections.frequency(elements, currentElement);
        }
        return elements.size() == count;
    }

    public static <E> boolean noneMatch(List<E> elements, Predicate<E> predicate) {
        return !anyMatch(elements, predicate);
    }

    public static <T, R> List<R> map(List<T> elements, Function<T, R> mappingFunction) {
        List<R> list = new ArrayList<R>();

        for (T currentElement : elements) {
            list.add(mappingFunction.apply(currentElement));
        }
        return list;
    }

    public static <E> Optional<E> max(List<E> elements, Comparator<E> comparator) {
        if (elements.isEmpty()) {
            return Optional.empty();
        }

        E max = null;
        for (int i = 0; i < elements.size(); i++) {
            if (i == 0) {
                max = elements.get(i);
            } else {
                int result = comparator.compare(max, elements.get(i));
                if (result < 0) {
                    max = elements.get(i);
                }
            }
        }
        return Optional.of(max);
    }

    public static <E> Optional<E> min(List<E> elements, Comparator<E> comparator) {
        if (elements.isEmpty()) {
            return Optional.empty();
        }

        E min = null;
        for (int i = 0; i < elements.size(); i++) {
            if (i == 0) {
                min = elements.get(i);
            } else {
                int result = comparator.compare(min, elements.get(i));
                if (result > 0) {
                    min = elements.get(i);
                }
            }
        }
        return Optional.of(min);
    }

    public static <E> List<E> distinct(List<E> elements) {
        Set<E> set = new HashSet<E>(elements);
        return new ArrayList<>(set);
    }

    public static <E> void forEach(List<E> elements, Consumer<E> consumer) {
        for (E currentElement : elements) {
            consumer.accept(currentElement);
        }
    }

    public static <E> Optional<E> reduce(List<E> elements, BinaryOperator<E> accumulator) {
        if (elements.isEmpty()) {
            return Optional.empty();
        }
        return Optional.ofNullable(reduce(elements.get(0), elements, accumulator));
    }

    public static <E> E reduce(E seed, List<E> elements, BinaryOperator<E> accumulator) {
        if (elements.isEmpty()) {
            return null;
        }

        E accumElements = null;
        int count = elements.indexOf(seed);

        for (int i = 0; i < elements.size(); i++) {
            if (i == count) {
                accumElements = elements.get(i);
            } else {
                accumElements = accumulator.apply(accumElements, elements.get(i));
            }
        }
        return accumElements;
    }

    public static <E> Map<Boolean, List<E>> partitionBy(List<E> elements, Predicate<E> predicate) {
        Map<Boolean, List<E>> map = new HashMap<>();
        List<E> resultTrue = new ArrayList<>();
        List<E> resultFalse = new ArrayList<>();

        for (E currentElement : elements) {
            if (predicate.test(currentElement)) {
                resultTrue.add(currentElement);
            } else {
                resultFalse.add(currentElement);
            }
            map.put(Boolean.TRUE, resultTrue);
            map.put(Boolean.FALSE, resultFalse);
        }
        return map;
    }

    public static <T, K> Map<K, List<T>> groupBy(List<T> elements, Function<T, K> classifier) {
        HashMap<K, List<T>> map = new HashMap<>();

        for (T currentElement : elements) {
            map.computeIfAbsent(classifier.apply(currentElement), k -> new ArrayList<>());
            map.get(classifier.apply(currentElement)).add(currentElement);
        }
        return map;
    }

    public static <T, K, U> Map<K, U> toMap(List<T> elements,
                                            Function<T, K> keyFunction,
                                            Function<T, U> valueFunction,
                                            BinaryOperator<U> mergeFunction) {
        HashMap<K, U> map = new HashMap<>();

        for (int i = 0; i < elements.size(); i++) {
            K key = keyFunction.apply(elements.get(i));
            U value = valueFunction.apply(elements.get(i));

            if (map.containsKey(key)) {
                value = mergeFunction.apply(map.get(key), value);
                map.put(key, value);
            } else {
                map.put(key, value);
            }
        }
        return map;
    }
}
