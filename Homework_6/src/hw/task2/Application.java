package hw.task2;

import java.util.Iterator;
import java.util.stream.Stream;

public class Application {

    public static void main(String[] args) {
        List<String> strings = new LinkedList<>();


        strings.add("John");
        strings.add("Joe");

        Iterator iterator = strings.iterator();
        while (iterator.hasNext()) {
            Object element = iterator.next();
            System.out.println(element);
        }
        Stream<String> stream = Stream.generate(() -> "test").limit(10);
    }
}


