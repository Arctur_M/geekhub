package hw.task2;

public interface List<E> extends Iterable<E> {
    void add(E element);

    boolean remove(E element);

    boolean remove(int index);

    E get(int index);

    int size();

    boolean isEmpty();
}
