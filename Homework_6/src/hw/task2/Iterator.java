package hw.task2;

public interface Iterator<E> extends java.util.Iterator {

    E next();

    boolean hasNext();
}
