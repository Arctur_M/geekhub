package hw.task2;

public class LinkedList<E> implements List<E> {

    private Node<E> head;
    private Node<E> tail;

    @Override
    public void add(E element) {
        Node<E> newElement = new Node<>(element, null);
        if (head == null) {
            head = newElement;
        } else {
            Node<E> currentElement = head;
            while (currentElement.next != null) {
                currentElement = currentElement.next;
            }
            currentElement.next = newElement;
        }
    }

    @Override
    public boolean remove(E element) {
        return false;
    }

    @Override
    public boolean remove(int index) {
        return false;
    }

    @Override
    public E get(int index) {
        return null;
    }

    @Override
    public int size() {
        return 0;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public Iterator iterator() {
        return new IteratorImpl(head);
    }

    private class IteratorImpl implements Iterator<E>, java.util.Iterator {
        Node<E> element;

        IteratorImpl(Node<E> element) {
            this.element = element;
        }

        @Override
        public E next() {
            E value = element.value;
            element = element.next;
            return value;
        }

        @Override
        public boolean hasNext() {
            return element != null;
        }
    }

    private static class Node<E> {
        E value;
        Node<E> next;

        Node(E value, Node<E> next) {
            this.value = value;
            this.next = next;
        }
    }
}