package hw.task2;

public interface Iterable<E> {

    hw.task2.Iterator iterator();
}